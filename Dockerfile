ARG jarFileName

FROM adoptopenjdk/openjdk11:latest

ADD build/libs/$jarFileName /opt/$jarFileName
EXPOSE 8080

ENTRYPOINT ["java", "-jar", "$jarFileName"]
