package com.example.generic.rest.template.demo.message.domain.ports;

public interface MessageExtendedRepository<T,ID> {
    <S extends T> S save(S entity, int ttl);
}
