package com.example.generic.rest.template.demo.message.adapters.api;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MessageDto {
    private String email;
    private String title;
    private String content;
    private Integer magicNumber;
}
