package com.example.generic.rest.template.demo.message.domain.ports;

import java.util.List;
import java.util.UUID;

import com.example.generic.rest.template.demo.message.domain.model.Message;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CassandraRepository<Message, UUID>, MessageExtendedRepository<Message,UUID> {
    List<Message> findAllByEmail(String email, Pageable page);
    List<Message> findAllByMagicNumber(Integer magicNumber);
}
