package com.example.generic.rest.template.demo.message.adapters.api;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SenderDto {
    private Integer magicNumber;
}
