package com.example.generic.rest.template.demo.message.domain.ports;


import com.example.generic.rest.template.demo.message.domain.ports.MessageExtendedRepository;
import com.example.generic.rest.template.demo.message.domain.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;

import java.util.UUID;

@RequiredArgsConstructor
public class MessageRepositoryImpl implements MessageExtendedRepository<Message, UUID> {
    private final CassandraOperations cassandraOperations;

    @Override
    public <S extends Message> S save(S entity, int ttl) {
        var insertOptions = InsertOptions.builder()
                .ttl(ttl)
                .build();
        cassandraOperations.insert(entity, insertOptions);
        return entity;
    }
}
