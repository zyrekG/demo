package com.example.generic.rest.template.demo.message.domain.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Delegate;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.*;

@Table("message")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @PrimaryKeyColumn(
            name = "id",
            ordinal = 0,
            type = PrimaryKeyType.PARTITIONED,
            ordering = Ordering.ASCENDING)
    private UUID id;
    @Indexed("email")
    private String email;
    @Indexed("magic_number")
    private Integer magicNumber;
    @Column("title")
    private String title;
    @Column("content")
    private String content;
}
