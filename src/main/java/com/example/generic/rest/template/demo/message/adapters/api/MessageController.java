package com.example.generic.rest.template.demo.message.adapters.api;

import com.example.generic.rest.template.demo.message.domain.model.Message;

import java.util.List;
import java.util.stream.Collectors;

import com.example.generic.rest.template.demo.message.domain.ports.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MessageController {
    private final MessageApiService messageApiService;

    @PostMapping("/message")
    public void message(@RequestBody MessageDto messageDto) {
        log.info("Creating message...");
        log.debug("Message:{}", messageDto);
        messageApiService.addMessage(messageDto);
        log.info("Created.");
    }

    @PostMapping("/send")
    public void send(@RequestBody SenderDto sender) {
        log.info("Sending message(s)...");
        log.debug("Sending message for magic number:{}", sender.getMagicNumber());
        messageApiService.sendMessages(sender);
        log.info("Message(s) sent");
    }

    @GetMapping("/messages/{emailValue}")
    public List<MessageDto> getMessages(@PathVariable String emailValue, @RequestBody PagingDto pagingDto) {
        log.info("Searching for message(s)...");
        log.debug("Provided e-mail for search:{}", emailValue);
        log.debug("Page:{}, Page size:{}",pagingDto.getPage(), pagingDto.getSize());
        return messageApiService.getAllMessages(emailValue, pagingDto.getPage(), pagingDto.getSize());
    }
}
