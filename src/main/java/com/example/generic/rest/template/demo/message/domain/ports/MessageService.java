package com.example.generic.rest.template.demo.message.domain.ports;

import com.example.generic.rest.template.demo.message.adapters.mail.MailService;
import com.example.generic.rest.template.demo.message.domain.model.Message;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public void addMessage(Message message){
        messageRepository.save(message,300);
    }


    public List<Message> getAllMessagesByMagicNumber(Integer number){
        return messageRepository.findAllByMagicNumber(number);

    }

    public List<Message> getAllMessagesByEmail(String email, Pageable pageable){
        return messageRepository.findAllByEmail(email, pageable);
    }

    public void deleteMessages(Iterable<Message> messages){
        messageRepository.deleteAll(messages);
    }
}
