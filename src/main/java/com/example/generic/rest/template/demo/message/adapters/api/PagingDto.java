package com.example.generic.rest.template.demo.message.adapters.api;

import lombok.Value;

@Value
public class PagingDto {
    int page;
    int size;
}
