package com.example.generic.rest.template.demo.message.adapters.api;

import com.example.generic.rest.template.demo.message.adapters.mail.MailService;
import com.example.generic.rest.template.demo.message.domain.model.Message;
import com.example.generic.rest.template.demo.message.domain.ports.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageApiService {

    @Delegate
    private final MessageService messageService;
    private final ModelMapper mapper;
    private final MailService mailService;

    public void addMessage(MessageDto messageDto) {
        log.debug("Trying to map {} to internal model", messageDto);
        var message = mapper.map(messageDto, Message.class);
        log.debug("Completed");
        message.setId(UUID.randomUUID());
        addMessage(message);
    }

    public List<MessageDto> getAllMessages(String email, int page, int size) {
        log.debug("Creating pageable from page: {}, size: {}", page, size);
        Pageable pageable = PageRequest.of(page, size);
        log.debug("Searching for messages belonging to e-mail {}:", email);
        var messages = getAllMessagesByEmail(email, pageable);
        log.info("Search completed.");
        log.debug("Found: {} results", messages.size());
        log.debug("Converting results to output object.");
        return messages.stream()
                .map(msg -> mapper.map(msg, MessageDto.class))
                .collect(Collectors.toList());
    }

    public void sendMessages(SenderDto magicNumber) {
        log.debug("Searching for messages with magic number: {}", magicNumber.getMagicNumber());
        var messages = getAllMessagesByMagicNumber(magicNumber.getMagicNumber());
        log.info("Search completed.");
        log.debug("Found: {} results", messages.size());
        log.info("Sending message(s)");
        log.debug("Messages: {}", messages);
        messages.forEach(mailService::sendMail);
        log.info("Deleting sent messages");
        deleteMessages(messages);
        log.info("Deleted messages.");
    }
}
