package com.example.generic.rest.template.demo.message.adapters.mail;

import com.example.generic.rest.template.demo.message.domain.model.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Slf4j
@Component
@RequiredArgsConstructor
public class MailService {
    private final JavaMailSender javaMailSender;

    @Async
    public void sendMail(Message message) {
        log.debug("Creating simple message from: {}", message);
        var msg = createSimpleMessage(message);
        log.debug("Created simple message: {}", msg);
        log.debug("Sending message...");
        javaMailSender.send(msg);
        log.debug("Message sent...");
    }

    private SimpleMailMessage createSimpleMessage(Message message) {
        var mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("mbo38952@gmail.com");
        mailMessage.setTo(message.getEmail());
        mailMessage.setSubject(message.getTitle());
        mailMessage.setText(message.getContent());
        return mailMessage;
    }
}
