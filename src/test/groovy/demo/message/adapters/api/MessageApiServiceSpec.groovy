package demo.message.adapters.api

import com.example.generic.rest.template.demo.message.adapters.api.MessageApiService
import com.example.generic.rest.template.demo.message.adapters.api.MessageDto
import com.example.generic.rest.template.demo.message.adapters.api.SenderDto
import com.example.generic.rest.template.demo.message.adapters.mail.MailService
import com.example.generic.rest.template.demo.message.domain.model.Message
import com.example.generic.rest.template.demo.message.domain.ports.MessageService
import org.modelmapper.ModelMapper
import spock.lang.Specification

class MessageApiServiceSpec extends Specification {
    private static final def CONTENT = "test"
    private static final def TITLE = "title"
    private static final def EMAIL = "email@test.com"
    private static final int MAGIC_NUMBER = 1

    MessageService messageService = Mock()
    ModelMapper mapper = Mock()
    MailService mailService = Mock()
    SenderDto senderDto = new SenderDto()
    def service = new MessageApiService(messageService, mapper, mailService)
    def messageDto = createMessageDto()

    def message = createMessage()


    def 'Should add message'() {
        when:
        service.addMessage(messageDto)
        then:
        1 * mapper.map(messageDto, Message.class) >> message
        1 * messageService.addMessage(message)
    }

    def 'Should send message'() {
        given:
        def messages = List.of(message)
        senderDto.magicNumber = 1
        when:
        service.sendMessages(senderDto)
        then:
        1 * messageService.getAllMessagesByMagicNumber(senderDto.magicNumber) >> messages
        1 * mailService.sendMail(message)
        1 * messageService.deleteMessages(messages)
    }

    private static createMessageDto(){
        def message = new MessageDto()
        message.setContent(CONTENT)
        message.setEmail(EMAIL)
        message.setMagicNumber(MAGIC_NUMBER)
        message.setTitle(TITLE)
        return message
    }

    private static createMessage(){
        return Message.builder()
        .content(CONTENT)
        .email(EMAIL)
        .magicNumber(MAGIC_NUMBER)
        .title(TITLE)
        .build()
    }
}
