package demo.message.adapters.mail

import com.example.generic.rest.template.demo.message.adapters.mail.MailService
import com.example.generic.rest.template.demo.message.domain.model.Message
import org.springframework.data.cassandra.core.CassandraOperations
import org.springframework.mail.javamail.JavaMailSender
import spock.lang.Specification

class MailServiceSpec extends Specification{
    JavaMailSender mailSender = Mock()
    def service = new MailService(mailSender)
    def message = Message.builder()
            .id(UUID.randomUUID())
            .content("Test")
            .email("test@test.com")
            .magicNumber(1)
            .title("title")
            .build()

    def 'Should send message'() {
        when:
        service.sendMail(message)
        then:
        1 * mailSender.send(_)
    }

}
