package demo.message.domain.ports

import com.example.generic.rest.template.demo.message.domain.model.Message
import com.example.generic.rest.template.demo.message.domain.ports.MessageRepositoryImpl
import org.springframework.data.cassandra.core.CassandraOperations
import org.springframework.data.cassandra.core.InsertOptions
import spock.lang.Specification

class MessageRepositoryImplSpec extends Specification {
    CassandraOperations cassandraOperations = Mock()
    def repository = new MessageRepositoryImpl(cassandraOperations)
    def message = Message.builder()
            .id(UUID.randomUUID())
            .content("Test")
            .email("test@test.com")
            .magicNumber(1)
            .title("title")
            .build()

    def 'Should add message'() {
        when:
        repository.save(message, 300)
        then:
        1 * cassandraOperations.insert(message,_)
    }

    def 'Should add null message'() {
        when:
        repository.save(null, 0)
        then:
        1 * cassandraOperations.insert(null,_)
    }
}
