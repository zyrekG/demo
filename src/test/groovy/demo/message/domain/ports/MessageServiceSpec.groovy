package demo.message.domain.ports

import com.example.generic.rest.template.demo.message.domain.model.Message
import com.example.generic.rest.template.demo.message.domain.ports.MessageRepository
import com.example.generic.rest.template.demo.message.domain.ports.MessageRepositoryImpl
import com.example.generic.rest.template.demo.message.domain.ports.MessageService
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Specification

class MessageServiceSpec extends Specification {
    MessageRepository messageRepository = Mock()
    def service = new MessageService(messageRepository)
    def message = Message.builder()
            .id(UUID.randomUUID())
            .content("Test")
            .email("test@test.com")
            .magicNumber(1)
            .title("title")
            .build()

    def 'Should add message'() {
        when:
        service.addMessage(message)
        then:
        1 * messageRepository.save(message, 300)
    }

    def 'Should delete message'() {
        given:
        def messages = List.of(message)
        when:
        service.deleteMessages(messages)
        then:
        1 * messageRepository.deleteAll(messages)
    }

    def 'Should get all messages by magic number'() {
        given:
        def magicNumber = 1
        def messages = List.of(message)

        when:
        def response = service.getAllMessagesByMagicNumber(magicNumber)
        then:
        1 * messageRepository.findAllByMagicNumber(magicNumber) >> messages
        response == messages
    }

    def 'Should get all messages by email and pageable'() {
        given:
        def email = "test@test.com"
        def pageable = PageRequest.of(1,1)
        def messages = List.of(message)

        when:
        def response = service.getAllMessagesByEmail(email, pageable)
        then:
        1 * messageRepository.findAllByEmail(email, pageable) >> messages
        response == messages
    }

}
